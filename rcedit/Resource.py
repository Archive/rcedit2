import gobject

class Resource (gobject.GObject):
	__gsignals__ = {
		'changed': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, ())
	}
	def __init__ (self, spec_type, val = None, recurse = 0):
		self.__gobject_init__ ()
		self.spec_type = spec_type
		self.val = val
		self.recurse = recurse
	def do_changed (self):
		pass

gobject.type_register (Resource)
