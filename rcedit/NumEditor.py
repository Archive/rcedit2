import gtk

class NumEditor (gtk.SpinButton):
	def __init__ (self, sc, r):
		gtk.SpinButton.__init__ (self)
		self.configure (gtk.Adjustment (0, -65535, 65535, 0.01), 0.01, 2)
		self.r = r
		self.set_value (self.r.val)
		self.is_int = 0
		self.connect ('value_changed', self.onValueChanged, None)
	def onValueChanged (self, b, data):
		self.r.val = self.get_value ()
		if self.is_int:
			self.r.val = int (self.r.val)
		self.r.emit ('changed')
