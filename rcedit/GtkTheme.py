from ResourceSpec import ResourceSpec
from SpecCollection import SpecCollection
from ThemeModel import ThemeModel
from Resource import Resource
from ColorCellRenderer import ColorCellRenderer
from BorderEditor import BorderEditor
from SizeEditor import SizeEditor
from ColorEditor import ColorEditor
from FontEditor import FontEditor
from ThemeColor import ThemeColor 
import string
from BasicTypes import *
from ViewHints import *
import gtk

class Engine (SpecCollection):
	def __init__ (self, name, parent):
		SpecCollection.__init__ (self)
		self.name = name
		self.parent = parent

color_specs = \
[ \
  ResourceSpec ('NORMAL', TYPE_STRING, 'Normal state', 0, ColorCellRenderer, ColorEditor), \
  ResourceSpec ('ACTIVE', TYPE_STRING, 'Active state', 0, ColorCellRenderer, ColorEditor), \
  ResourceSpec ('PRELIGHT', TYPE_STRING, 'Prelight state', 0, ColorCellRenderer, ColorEditor), \
  ResourceSpec ('SELECTED', TYPE_STRING, 'Selected state', 0, ColorCellRenderer, ColorEditor), \
  ResourceSpec ('INSENSITIVE', TYPE_STRING, 'Insensitive state', 0, ColorCellRenderer, ColorEditor), \
]

class ColorSetting (SpecCollection):
	def __init__ (self, parent):
		SpecCollection.__init__ (self)
		self.parent = parent
		for s in color_specs:
			self.addSpec (s)
	def addResource (self, r, n):
		SpecCollection.addResource (self, r, n)

TYPE_GTK_ENGINE = 'gtk-engine'
TYPE_GTK_COLOR_SETTING = 'gtk-color-setting'
TYPE_GTK_BORDER = 'gtk-border'
TYPE_GTK_SIZE = 'gtk-size'

class BorderSpec (ResourceSpec):
	def __init__ (self, spec_type):
		ResourceSpec.__init__ (self, key=spec_type, type=TYPE_GTK_BORDER, editor=BorderEditor) 

class SizeSpec (ResourceSpec):
	def __init__ (self, spec_type):
		ResourceSpec.__init__ (self, key=spec_type, type=TYPE_GTK_SIZE, editor=SizeEditor) 


specs = \
[ \
  ResourceSpec ('font_name', TYPE_FONT, 'Font', 0, None, FontEditor), \
  ResourceSpec ('font', TYPE_FONT, 'Font', 0, None, FontEditor), \
  ResourceSpec ('applies-to', TYPE_LIST, 'Apply style to', VIEW_HINT_HIDDEN), \
  ResourceSpec ('engine', TYPE_GTK_ENGINE, 'Drawing Engine', VIEW_HINT_MERGE_CHILDREN), \
  ResourceSpec ('bg', TYPE_GTK_COLOR_SETTING, 'Background color', VIEW_HINT_MERGE_CHILDREN | VIEW_HINT_NO_VALUE), \
  ResourceSpec ('fg', TYPE_GTK_COLOR_SETTING, 'Foreground color', VIEW_HINT_MERGE_CHILDREN | VIEW_HINT_NO_VALUE), \
  ResourceSpec ('base', TYPE_GTK_COLOR_SETTING, 'Base color', VIEW_HINT_MERGE_CHILDREN | VIEW_HINT_NO_VALUE), \
  ResourceSpec ('text', TYPE_GTK_COLOR_SETTING, 'Text color', VIEW_HINT_MERGE_CHILDREN | VIEW_HINT_NO_VALUE), \
]

STATE_ROOT = 1
STATE_READ_STYLE_NAME = 2
STATE_IN_STYLE = 3
STATE_READ_ENGINE_NAME = 4
STATE_IN_ENGINE = 5
STATE_READ_VALUE = 6
STATE_READING_LIST = 7
STATE_READ_CLASS = 8
STATE_READ_CLASS_STYLE = 9
STATE_IGNORE_STYLE = 10

class GtkTheme (ThemeModel):
	def __init__ (self):
		ThemeModel.__init__ (self, 'gtk')
		for s in specs:
			self.addSpec (s)
	def isNumeric (self, v):
		try:
			string.atof(v)
			return 1
		except:
			return 0
	def printList (self, l):
		print "{",
		for i in range (len (l)):
			if i: print ',',
			print l[i],
		print "}"
	def printVal (self, sc, r):
		if self.isNumeric (r.val) or (sc.specs.has_key (r.spec_type) and sc.specs[r.spec_type].type == TYPE_ENUM):
			print r.val
		elif type (r.val) == type ([]):
			self.printList (r.val)
		else:
			print '\"%s\"' % (r.val)
	def printColors (self, sc, r, indent):
		for i in r.val.resources[r.spec_type]:
			print "%s%s[%s] = %s" % (indent, r.spec_type, i.spec_type, r.val.toString ())
	def printResource (self, sc, r, indent = '\t'):
		if (sc.specs.has_key (r.spec_type) and sc.specs[r.spec_type].type == TYPE_GTK_COLOR_SETTING):
			self.printColors (sc, r, indent)
		else:
			print '%s%s =' % (indent, r.spec_type),
			self.printVal (sc, r)
	def printEngine (self, v):
		print '\tengine \"%s\" {' % (v.val.name)
		keys = v.val.resources.keys ()
		keys.sort ()
		for key in keys:
			for r in v.val.resources[key]:
				self.printResource (v.val, r, '\t\t')
		print '\t}'
	def save (self, dir):
		keys = self.resources.keys ()
		keys.sort ()
		for key in keys:
			print "style \"%s\" {" % (key)
			applies = [] 
			for v in self.resources[key]:
				if v.spec_type == 'applies-to':
					applies = v.val
				elif v.spec_type == 'engine':
					self.printEngine (v)
				else:
					self.printResource (self, v)
			print "}"
			for a in applies:
				if string.find (a, '.') != -1:
					print "widget_class",
				else:
					print "class",
				print "\"%s\" style \"%s\"" % (a, key)
	def simpleTokenize (self, buf):
		tokens = []
		current = ''
		quote_open = 0
		for i in buf:
			if (i == ' ' or i == '\n' or i == '\t' or i == ',') and not quote_open:
				if current == '': continue
				tokens.append (current)
				current = ''
			else:
				current = current + i
			if i == '\"':
				quote_open = not quote_open
		if current != '': tokens.append (current)
		return tokens
	def stripQuotes (self, str):
		if (str[0] == '\"' and str[len(str) - 1]) == '\"':
			return str[1:len(str) - 1]
		else:
			return str
	def ensureSpec (self, sc, r):
		if sc.specs.has_key (r.spec_type): return sc.specs[r.spec_type]
		spec = ResourceSpec (r.spec_type)
		if type (r.val) == type ([]):
			if len (r.val) == 4:
				spec = BorderSpec (r.spec_type)
			elif len (r.val) == 2:
				spec = SizeSpec (r.spec_type)
			else:
				spec.type = TYPE_LIST
		elif self.stripQuotes (r.val) == r.val:
			if self.isNumeric (r.val):
				old = r.val
				r.val = string.atof (old)
				if string.find (old, '.') == -1:
					spec.type = TYPE_INT
					r.val = int (r.val)
				else:
					spec.type = TYPE_FLOAT
			else:
				spec.type = TYPE_ENUM
		else:
			spec.type = TYPE_STRING
		sc.addSpec (spec)
		return spec
	def findResource (self, sc, style_name, key):
		r = None
		if sc.resources.has_key (style_name):
			l = sc.resources[style_name]
			for rs in l:
				if rs.spec_type == key:
					r = rs
					break
		if not r:
			r = Resource (key)
			sc.addResource (r, style_name)
		return r
	def splitKey (self, str, ret):
		index = 0
		for i in range (len (str)):
			if str[i] == '[':
				index = i
				break
		if index:
			ret[0] = str[0:index]
			ret[1] = str[index + 1: len(str) - 1]
			return 1
		else:
			return 0
	def load (self, dir):
		filename = dir + '/gtk-2.0/gtkrc'
		f = open(filename, 'r')
		tokens = self.simpleTokenize (f.read ())
		f.close ()
		
		states = [STATE_ROOT]
		style_name = None
		engine = None
		engine_name = None
		r = None
		class_name = None
		sc = [self]
		for i in range (len(tokens)):
			s = states[len(states) - 1]
			if s == STATE_ROOT and tokens[i] == 'style':
				states.append (STATE_READ_STYLE_NAME)
			elif s == STATE_READ_STYLE_NAME:
				style_name = self.stripQuotes (tokens[i])
				states.pop ()
			elif s == STATE_ROOT and tokens[i] == '{':
				states.append (STATE_IN_STYLE)
			elif s == STATE_IN_STYLE and tokens[i] == '{':
				states.append (STATE_IN_ENGINE)
				engine = Engine (engine_name, sc[len (sc) - 1])
				sc.append (engine)
			elif (s == STATE_IN_ENGINE) and tokens[i] == '}':
				states.pop ()
				self.addResource (Resource ('engine', engine, 1), style_name) 
				sc.pop () 
			elif (s == STATE_IN_STYLE) and tokens[i] == '}':
				states.pop ()
			elif s == STATE_IN_STYLE and tokens[i] == 'engine':
				states.append (STATE_READ_ENGINE_NAME)
			elif s == STATE_READ_ENGINE_NAME:
				engine_name = self.stripQuotes (tokens[i])
				states.pop ()
			elif (s == STATE_IN_STYLE or s == STATE_IN_ENGINE) and tokens[i] == '=':
				ret = [None, None]
				key = None
				coll = sc[len (sc) - 1]
				sc_key = None
				if self.splitKey (tokens[i - 1], ret):
					rs = self.findResource (coll, style_name, ret[0])
					if not rs.val:
						rs.val = ColorSetting (coll)
						rs.recurse = 1
					coll = rs.val
					sc_key = ret[0]
					key = ret[1]
				else:
					sc_key = style_name
					key = tokens[i - 1]
				
				states.append (STATE_READ_VALUE)
				r = Resource (key)
				sc.append (coll)
				coll.addResource (r, sc_key)
			elif s == STATE_READ_VALUE:
				if tokens[i] == '{':
					states.append (STATE_READING_LIST)
					r.val = []
				else:
					r.val = tokens[i]
					coll = sc.pop ()
					spec = self.ensureSpec (coll, r)
					if spec.type != TYPE_INT and spec.type != TYPE_FLOAT:
						r.val = self.stripQuotes (r.val)
					if type (coll) == type (ColorSetting (None)):
						r.val = ThemeColor (str=r.val)
					states.pop ()
			elif s == STATE_READING_LIST:
				if tokens[i] == '}':
					coll = sc.pop ()
					if type (coll) == type (ColorSetting (None)):
						r.val = ThemeColor (str=r.val)
					else:
						spec = self.ensureSpec (coll, r)
					states.pop ()
					states.pop ()
				elif tokens[i] != ',':
					old = tokens[i]
					val = string.atof (old)
					if string.find (old, '.') == -1:
						val = int (val)
					r.val.append (val)
			elif s == STATE_ROOT and (tokens[i] == 'class' or tokens[i] == 'widget_class'):
				states.append (STATE_READ_CLASS)
			elif s == STATE_READ_CLASS:
				class_name = self.stripQuotes (tokens[i])
				states.pop ()
				states.append (STATE_IGNORE_STYLE)
			elif s == STATE_IGNORE_STYLE and tokens[i] == 'style':
				states.pop ()
				states.append (STATE_READ_CLASS_STYLE)
			elif s == STATE_READ_CLASS_STYLE:
				class_style_name = self.stripQuotes (tokens[i])
				r = None
				if self.resources.has_key (class_style_name):
					l = self.resources[class_style_name]
					for r in l:
						if r.spec_type == 'applies-to': break
						r = None
				if r: 
					r.val.append (class_name)
				else:
					r = Resource ('applies-to', [ class_name ] )
					self.addResource (r, class_style_name)
				states.pop ()
	def stringifyResource (self, r):
		if (r.spec_type == 'engine'):
			return r.val.name
		else:
			return SpecCollection.stringifyResource (self, r)
	def unstringifyResource (self, str, r):
		if (r.spec_type == 'engine'):
			r.val.name = str
		else:
			SpecCollection.unstringifyResource (self, str, r)
