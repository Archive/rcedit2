class ResourceSpec:
	"""Specification for resource metadata.
	
	The following members may be set or passed in at construction time:
		key: Associate with resources of this spec_type.
		type: The type of data stored in the resource value field.
		      See BasicTypes.py for the basic data types.
		desc: A short description of the resource type.
		hints: A bitfield of rendering hints. See ViewHints.py for
		       the standard hints.
		cell_renderer: Python class type of a custom value renderer.
			       This must subclass gtk.GenericCellRenderer
			       and support a constructor with 0 arguments. 
		editor: Python class type of a custom value editor.
			This must subclass gtk.Widget and support a
			constructor with 2 arguments: a SpecCollection
			containing the resource, and the resource to edit.
	"""
	def __init__ (self, key = None, type = None, desc = None, hints = 0, cell_renderer = None, editor = None):
		self.key = key
		self.type = type
		self.desc = desc
		self.hints = hints
		self.cell_renderer = cell_renderer
		self.editor = editor 
