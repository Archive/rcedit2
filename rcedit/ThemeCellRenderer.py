import gobject
import gtk
from Resource import Resource
from SpecCollection import SpecCollection
from ViewHints import *

class ThemeCellRenderer (gtk.GenericCellRenderer):
	__gproperties__ = {
		'resource': (Resource, 'foo property', 'the foo of the object', gobject.PARAM_READWRITE),
		'sc': (SpecCollection, 'foo property', 'the foo of the object', gobject.PARAM_READWRITE),
	}

	def __init__ (self):
		self.__gobject_init__ ()
		self.r = None
		self.sc = None
		self.renderers = {}
		self.default = gtk.CellRendererText ()
	def do_set_property (self, pspec, value):
		if pspec.name == 'resource':
			self.r = value
		elif pspec.name == 'sc':
			self.sc = value
		else:
			raise AttributeError, 'unknown property %s' % pspec.name
		    
		#rtype = None
		#if sc.specs.has_key (r.spec_type):
		#	rtype = sc.specs[r.spec_type].cell_renderer
		#if not rtype:
		#	rtype = gtk.CellRendererText ()
		#self.renderer = rtype ()
	def do_get_property (self, pspec):
		if pspec.name == 'resource':
			return self.r
		elif pspec.name == 'sc':
			return self.sc
		else:
			raise AttributeError, 'unknown property %s' % pspec.name
	def getRenderer (self):
		if not self.r:
			self.default.set_property ('text', '')
			return self.default
		renderer = None
		if self.renderers.has_key (self.r):
			renderer = self.renderers[self.r]
		elif self.sc and self.sc.specs.has_key (self.r.spec_type) and self.sc.specs[self.r.spec_type].cell_renderer:
			renderer = self.sc.specs[self.r.spec_type].cell_renderer ()
			self.renderers[self.r] = renderer
		if renderer:
			renderer.set_property ('resource', self.r)
			renderer.set_property ('sc', self.sc)
			return renderer
		else:
			if self.sc:
				if self.sc.specs.has_key (self.r.spec_type) and self.sc.specs[self.r.spec_type].hints & VIEW_HINT_NO_VALUE:
					val = ""
				else:
					val = self.sc.stringifyResource (self.r)
			else:
				val = self.r.val
			self.default.set_property ('text', val)
			return self.default
	def on_get_size (self, widget, cell_area):
		ret = self.getRenderer ().get_size (widget)
		return ret[1:5]
	def on_render (self, window, widget, background_area, cell_area, expose_area, flags):
		self.getRenderer ().render (window, widget, background_area, cell_area, expose_area, flags)
	def on_activate (self, event, widget, path, background_area, cell_area, flags):
		self.getRenderer ().activate (event, widget, path, background_area, cell_area, flags)
	def on_start_editing (self, event, widget, path, background_area, cell_area, flags):
		return self.getRenderer ().start_editing (event, widget, path, background_area, cell_area, flags)

gobject.type_register (ThemeCellRenderer)
