import gtk
import string

class ThemeColor:
	def __init__ (self, r = 0, g = 0, b = 0, str = None):
		if str:
			self.parse (str)
		else:
			self.red = r
			self.green = g
			self.blue = b
	def parse (self, str):
		if type(str) == type([]):
			self.red = int (string.atof (str[0]) * 65536)
			self.green = int (string.atof (str[1]) * 65536)
			self.blue = int (string.atof (str[2]) * 65536)
		else:
			color = gtk.gdk.color_parse (str)
			self.red = color.red
			self.green = color.green
			self.blue = color.blue
	def toString (self):
		return "#%02x%02x%02x" % (self.red >> 8, self.green >> 8, self.blue >> 8)
