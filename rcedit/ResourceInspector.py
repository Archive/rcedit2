import gtk
from TextEditor import TextEditor
from NumEditor import NumEditor
from IntEditor import IntEditor
from BasicTypes import *
from ViewHints import *

class ResourceInspector (gtk.VBox):
	def __init__ (self):
		gtk.VBox.__init__ (self)
		self.set_border_width (8)
		self.set_spacing (4)
		
		hbox = gtk.HBox (0, 0)
		self.desc_label = gtk.Label ('')
		self.desc_label.set_justify (gtk.JUSTIFY_LEFT)
		hbox.pack_start (self.desc_label, 0, 0, 0)
		self.pack_start (hbox, 0, 0, 0)

		label = gtk.Label ('')
		label.set_property ('xalign', 0.0)
		label.set_markup ('<span weight=\"bold\" size=\"larger\">Value:</span>')
		self.pack_start (label, 0, 0, 0)
		self.edit_label = label
		
		self.r = None
		self.sc = None
		self.editor = None
		self.updateLabels ()
	def updateLabels (self):
		str = ""
		if self.r and self.sc and self.sc.specs.has_key (self.r.spec_type):
			str = self.sc.specs[self.r.spec_type].desc
		self.desc_label.set_markup ("<span weight=\"bold\" size=\"larger\">Description:</span>\n%s" % (str))
	def inspect (self, r, sc):
		self.r = r
		self.sc = sc
		self.updateLabels ()
		editor_type = None

		if self.r and self.sc and self.sc.specs.has_key (self.r.spec_type):
			spec = self.sc.specs[self.r.spec_type]
		else:
			spec = None

		if not self.r or spec and spec.hints & VIEW_HINT_NO_VALUE:
			self.edit_label.hide ()
			if self.editor: self.editor.destroy ()
			return

		self.edit_label.show ()
		
		if spec:
			editor_type = spec.editor
			if not editor_type:
				if spec.type == TYPE_INT:
					editor_type = IntEditor
				elif spec.type == TYPE_FLOAT:
					editor_type = NumEditor
				
		if self.editor:
			self.editor.destroy ()
			
		if editor_type:
			self.editor = editor_type (sc, r)
		else:
			self.editor = TextEditor (sc, r)
		
		self.editor.show ()
		self.pack_start (self.editor, 0, 0, 0)
