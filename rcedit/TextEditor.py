import gtk

class TextEditor (gtk.Entry):
	def __init__ (self, sc, r):
		gtk.Entry.__init__ (self)
		self.set_text (sc.stringifyResource (r))
