from NumEditor import NumEditor
import gtk

class IntEditor (NumEditor):
	def __init__ (self, sc, r):
		NumEditor.__init__ (self, sc, r)
		self.is_int = 1
		self.configure (gtk.Adjustment (self.r.val, -65535, 65535, 1), 1, 0)
