import gnome.ui
import pango

# grr
PANGO_SCALE = 1024

class FontEditor (gnome.ui.FontPicker):
	def __init__ (self, sc, r):
		gnome.ui.FontPicker.__init__ (self)
		self.set_property ('mode', gnome.ui.FONT_PICKER_MODE_FONT_INFO)
		self.set_property ('use-font-in-label', 1)

		self.r = r
		self.set_font_name (self.r.val)
		self.setFontSize ()
		
		self.connect ('font_set', self.onFontSet, None)
		
	def onFontSet (self, picker, font_name, data):
		self.r.val = font_name 
		self.setFontSize ()
		self.r.emit ('changed')
		
	def setFontSize (self):
		desc = pango.FontDescription (self.r.val)
		size = desc.get_size () / PANGO_SCALE 
		self.set_property ('label-font-size', size)
