import bonobo
import bonobo.ui
import gnome.ui
import gtk

from GtkTheme import GtkTheme
from ThemeTreeView import ThemeTreeView
from ResourceInspector import ResourceInspector 

RCEDIT_UI_XML = "rcedit_shell.xml"

class AppShell (bonobo.ui.Window):
	def __init__ (self):
		bonobo.ui.Window.__init__ (self, 'RCEdit', 'rcedit')
	
		# gui shell
		ui_container = self.get_ui_container ()
		ui_component = bonobo.ui.Component ('rcedit')
		ui_component.set_container (ui_container.corba_objref ())
		bonobo.ui.util_set_ui (ui_component, '', RCEDIT_UI_XML,
				       'rcedit')
				       
		verbs = [
			('FileNew', self.onFileNew),
			('FileOpen', self.onFileOpen),
			('FileExit', self.onFileExit),
			('HelpAbout', self.onHelpAbout)
		]
		ui_component.add_verb_list (verbs, self)

		# components
		self.theme = GtkTheme ()
		#self.theme.load ('/home/hestgray/.themes/R2')
		self.theme.load ('/gnome/head/INSTALL/share/themes/Metal')
		hpaned = gtk.HPaned ()
		self.tree = ThemeTreeView (self.theme)
		self.set_size_request (500, 400)
		self.set_resizable (1)
		hpaned.add1 (self.tree)

		self.inspector = ResourceInspector ()
		hpaned.add2 (self.inspector)

		self.set_contents (hpaned)
	
		self.tree.tree_view.get_selection ().connect ('changed', self.selectionCb, None)
	
	def selectionCb (self, selection, data):
		iter = selection.get_selected ()
		r = iter[0].get_value (iter[1], 1)
		sc = iter[0].get_value (iter[1], 2)
		self.inspector.inspect (r, sc)
	def onFileNew (self, uic, verb, win):
		app = AppShell ()
		app.show_all ()
		
	def onFileOpen (self, uic, verb, win):
		fsel = gtk.FileSelection ('Please select a theme to edit.')
		fsel.ok_button.connect ('clicked', self.loadFile, fsel)
		fsel.cancel_button.connect ('clicked', lambda button, fsel: fsel.destroy (), fsel)
		fsel.show_all ()
	def onFileExit (self, uic, verb, win):
		bonobo.main_quit ()
	def loadFile (self, button, fsel):
		self.theme = GtkTheme ()
		self.theme.load (fsel.get_filename ())
		fsel.destroy ()
	def onHelpAbout (self, uic, verb, win):
		about = gnome.ui.About ('RCEdit', '0.0.1',
				    	'Copyright (C) 2002 Rachel Hestilow',
					'RCEdit is an all-purpose theme editor.',
					['Rachel Hestilow (hestilow@ximian.com'],
					[], 'GNOME Translation Team')
		about.show_all ()

if __name__ == '__main__':
	app = AppShell ()
	app.show_all ()
	bonobo.main ()
