import gtk
from ThemeTreeModel import ThemeTreeModel
from ThemeCellRenderer import ThemeCellRenderer

class ThemeTreeView (gtk.ScrolledWindow):
	def __init__ (self, theme):
		gtk.ScrolledWindow.__init__ (self)
	
		self.model = ThemeTreeModel (theme)
		self.tree_view = gtk.TreeView (self.model)
		
		self.cell1 = gtk.CellRendererText()
		column = gtk.TreeViewColumn ("Resource", self.cell1, text=0)
		self.tree_view.append_column (column)

		self.cell2 = ThemeCellRenderer ()
		column = gtk.TreeViewColumn ("Value", self.cell2, resource=1, sc=2)
		self.tree_view.append_column (column)
	
		self.set_policy (gtk.POLICY_AUTOMATIC, gtk.POLICY_AUTOMATIC)
		self.add (self.tree_view)
	def reload (self):
		self.model.reload ()
