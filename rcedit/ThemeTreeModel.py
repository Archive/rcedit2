import gtk, gobject
from Resource import Resource
from SpecCollection import SpecCollection 
from ViewHints import *

class ThemeTreeModel (gtk.TreeStore):
	def __init__ (self, theme):
		self.theme = theme
		gtk.TreeStore.__init__ (self, gobject.TYPE_STRING, Resource, SpecCollection)
		#gtk.TreeStore.__init__ (self, gobject.TYPE_STRING)
		self.theme.connect ('resource_added', self.onResourceAdded, None)
		#self.theme.connect ('resource_removed', self.onResourceRemoved, None)
		self.iters = {}
		self.reload ()
	def reload (self):
		self.iters = {}
		self.clear ()
		self.addItems (self.theme, None)
	def addItems (self, sc, parent, pretend_r = None, pretend_sc = None):
		l = sc.resources.keys ()
		l.sort ()
		for k in l:
			iter = self.append (parent)
			if pretend_r and pretend_sc:
				self.set_value (iter, 0, pretend_r.spec_type)
				self.set_value (iter, 1, pretend_r)
				self.set_value (iter, 2, pretend_sc)
				self.iters[pretend_r] = iter
			else:
				self.set_value (iter, 0, k)
				self.set_value (iter, 2, sc)
				self.iters[sc] = iter
			for r in sc.resources[k]:
				no_append = 0
				if sc.specs.has_key (r.spec_type):
					spec = sc.specs[r.spec_type]
					if spec.hints & VIEW_HINT_MERGE_CHILDREN:
						no_append = 1
					elif spec.hints & VIEW_HINT_HIDDEN:
						continue
				self.addItem (sc, iter, r, no_append)
	def addItem (self, sc, parent, r, no_append = 0):
		if no_append:
			iter = parent
			pretend_r = r
			pretend_sc = sc
		else:
			iter = self.append (parent)
			self.iters[r] = iter 
			self.set_value (iter, 0, r.spec_type)
			self.set_value (iter, 1, r)
			self.set_value (iter, 2, sc)
			pretend_r = None
			pretend_sc = None
		if r.recurse:
			self.addItems (r.val, iter, pretend_r, pretend_sc)
		r.connect ('changed', self.onResourceChanged, None)
		return iter
	def onResourceAdded (self, sc, key, r, data):
		pass
	def onResourceChanged (self, r, data):
		iter = self.iters[r]
		self.set_value (iter, 1, r)
