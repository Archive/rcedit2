import gtk
import gnome.ui
from ThemeColor import ThemeColor

class ColorEditor (gtk.VBox):
	def __init__ (self, sc, r):
		gtk.VBox.__init__ (self)
		self.set_spacing (4)
		self.picker = gnome.ui.ColorPicker ()
		self.pack_start (self.picker, 0, 0, 0)
		self.label = gtk.Label ('')
		self.pack_start (self.label, 0, 0, 0)
		
		self.picker.set_i16 (r.val.red, r.val.green, r.val.blue, 0)
		self.picker.connect ('color_set', self.onColorSet, None)
		self.r = r
		self.updateLabel ()

		self.label.show ()
		self.picker.show ()
	def updateLabel (self):
		c = self.r.val
		self.label.set_text ("Hex value: %s" % (c.toString ()))
	def onColorSet (self, picker, r, g, b, a, data):
		self.r.val.red = r
		self.r.val.green = g
		self.r.val.blue = b
		self.updateLabel ()
		self.r.emit ('changed')
