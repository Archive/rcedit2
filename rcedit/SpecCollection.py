from Resource import Resource
import gobject

class SpecCollection (gobject.GObject):
	"""Resource container.

	This class implements storage of resource objects. It also includes a list of specifications for registered resource types. Signals "resource_added" and resource_removed" are emitted upon adding/removing a resource.
	
	Theme implementations should derive any subcontainers from this class if they want the subresources to appear in the editor tree.
	
	Theme implementations may also override the methods stringifyResource and unstringifyResource if they wish to implement alternate storage of Resource values.
	"""
	__gsignals__ = {
		'resource_added': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_STRING, Resource)),
		'resource_removed': (gobject.SIGNAL_RUN_LAST, gobject.TYPE_NONE, (gobject.TYPE_STRING, Resource))
	}
	
	def __init__ (self):
		self.__gobject_init__ ()
		self.specs = {} 
		self.resources = {} 
		self.frozen = 0
		self.changed = 0
		self.parent = None
	def do_resource_added (self, key, r):
		pass
		
	def do_resource_removed (self, key, r):
		pass

	def addResource (self, r, key):
		if self.resources.has_key (key):
			self.resources[key].append (r)
		else:
			self.resources[key] = [ r ]
		self.emit ('resource_added', key, r)

	def removeResource (self, key):
		if self.resources.has_key (key):
			self.resources[key].remove (r)
		self.emit ('resource_removed', key, r)

	def addSpec (self, spec):
		self.specs[spec.key] = spec
		
	def removeSpec (self, key):
		if self.specs.has_key (key):
			del self.specs[key]
			
	def stringifyResource (self, r):
		return str (r.val)
	def unstringifyResource (self, str, r):
		r.val = str

gobject.type_register (SpecCollection)
