import gobject
import gtk
from Resource import Resource
from SpecCollection import SpecCollection
from ThemeColor import ThemeColor

class ColorCellRenderer (gtk.GenericCellRenderer):
	__gproperties__ = {
		'resource': (Resource, 'foo property', 'the foo of the object', gobject.PARAM_READWRITE),
		'sc': (SpecCollection, 'foo property', 'the foo of the object', gobject.PARAM_READWRITE),
	}

	def __init__ (self):
		self.__gobject_init__ ()
		self.r = None
		self.sc = None
		self.renderer = gtk.CellRendererPixbuf ()
	def do_set_property (self, pspec, value):
		if pspec.name == 'resource':
			self.r = value
		elif pspec.name == 'sc':
			self.sc = value
		else:
			raise AttributeError, 'unknown property %s' % pspec.name
		    
	def do_get_property (self, pspec):
		if pspec.name == 'resource':
			return self.r
		elif pspec.name == 'sc':
			return self.sc
		else:
			raise AttributeError, 'unknown property %s' % pspec.name
	def redraw (self, cell_area):
		tcolor = self.r.val
		color = long ((tcolor.red >> 8) << 16 | (tcolor.green >> 8) << 8 | (tcolor.blue >> 8))
		src = gtk.gdk.Pixbuf (gtk.gdk.COLORSPACE_RGB, 1, 8, 1, 1)
		dest = src.composite_color_simple (cell_area[2] * 0.75, cell_area[3], gtk.gdk.INTERP_NEAREST, 0, 65535, color, color)
		self.renderer.set_property ('pixbuf', dest)
	def on_get_size (self, widget, cell_area):
		return tuple ([0, 0, 24, 8]) 
	def on_render (self, window, widget, background_area, cell_area, expose_area, flags):
		self.redraw (cell_area)
		self.renderer.render (window, widget, background_area, cell_area, expose_area, flags)
	def on_activate (self, event, widget, path, background_area, cell_area, flags):
		self.renderer.activate (event, widget, path, background_area, cell_area, flags)
	def on_start_editing (self, event, widget, path, background_area, cell_area, flags):
		return self.renderer.start_editing (event, widget, path, background_area, cell_area, flags)

gobject.type_register (ColorCellRenderer)
